import java.util.*;

class Jeu{

	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Quel est la taille de votre plateau?");
		String taille = sc.nextLine();
		int t = Integer.parseInt(taille);
		Plateau pl = new Plateau(t);
		pl.creerTableau();
		boolean rejouer = true;
		while(rejouer == true){
			System.out.println("A quel mode de jeux voulez-vous jouer?");
			System.out.println("1 : Deux joueurs");
			System.out.println("2 : Un joueur");
			String mode = sc.nextLine();
			t  = Integer.parseInt(mode);
			pl.afficherPlateau();
			if(t == 1){
				pl.joueDeuxHumains();
			}
			else if(t == 2){
				pl.joueOrdiHumain();
			}
			System.out.println("Voulez-vous rejouer?");
			System.out.println("1 : Oui");
			System.out.println("2 : Non");
			String jouer = sc.nextLine();
			t  = Integer.parseInt(jouer);
			if(t != 1){
				rejouer = false;
				System.out.println("Au revoir");
			}
		}


		


	}
}