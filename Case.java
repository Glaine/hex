import java.util.*;

class Case{
	private int etat;
	private int posI;
	private int posJ;
	private int posIRep;
	private int posJRep;
	private Vector<Case> fils;

	public Case(int e, int i, int j, int ir, int jr){
		etat = e;
		posI = i;
		posJ = j;
		posIRep = ir;
		posJRep = jr;
		fils = new Vector<Case>();
	}

	public int getEtat(){
		return etat;
	}

	public void setEtat(int e){
		etat = e;
	}

	public int getPosI(){
		return posI;
	}

	public int getPosJ(){
		return posJ;
	}

	public int getPosIRep(){
		return posIRep;
	}

	public int getPosJRep(){
		return posJRep;
	}

	public void setPosRep(int i, int j){
		posIRep = i;
		posJRep = j;
	}

	public Vector<Case> getFils(){
		return fils;
	}

	public void addFils(Case f){
		fils.add(f);
	}

}