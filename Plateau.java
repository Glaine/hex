import java.util.*;



class Plateau{
	private int taille;
	private Case[][] plateau;


	public Plateau(int t){
		taille = t;
	}

	//fini
	public boolean ajouterPion(int i, int j, int c){
		if(plateau[i][j].getEtat() == 0){
			plateau[i][j].setEtat(c);
			int[][] listeVoisins = voisins(i,j,c);
			int cpt = 0;
			for(int k = 0 ; k < listeVoisins.length ; ++k){
				if(listeVoisins[k][0] != -1){
					cpt += 1;
				}
			}
			if(cpt == 1){
				for(int x = 0; x < listeVoisins.length; ++x){
					if(listeVoisins[x][0] != -1){
						plateau[i][j].setPosRep(listeVoisins[x][0], listeVoisins[x][1]);
					}
				}
				Case rep = plateau[plateau[i][j].getPosIRep()][plateau[i][j].getPosJRep()];
				rep.addFils(plateau[i][j]);
			}
			if(cpt == 2){
				boolean arret = false;
				int x = 0;
				while(arret == false){
					if(listeVoisins[x][0] != -1){
						plateau[i][j].setPosRep(listeVoisins[x][0], listeVoisins[x][1]);
						arret = true;
					}
				}
				Case rep = plateau[plateau[i][j].getPosIRep()][plateau[i][j].getPosJRep()];
				rep.addFils(plateau[i][j]);
				Case rep2 = plateau[listeVoisins[x+1][0]][listeVoisins[x+1][1]];
				for(int k = 0; k < rep2.getFils().size(); ++k){
					rep2.getFils().elementAt(k).setPosRep(rep.getPosI(), rep.getPosJ());
					rep.addFils(rep2.getFils().elementAt(k));
				}
				rep2.getFils().removeAllElements();
				rep2.setPosRep(rep.getPosI(), rep.getPosJ());
				rep.addFils(rep2);
			}
			if(cpt == 3){
				plateau[i][j].setPosRep(listeVoisins[0][0], listeVoisins[0][1]);
				Case rep = plateau[plateau[i][j].getPosIRep()][plateau[i][j].getPosJRep()];
				Case rep2 = plateau[listeVoisins[1][0]][listeVoisins[1][1]];
				Case rep3 = plateau[listeVoisins[2][0]][listeVoisins[2][1]];
				for(int k = 0; k < rep2.getFils().size(); ++k){
					rep2.getFils().elementAt(k).setPosRep(listeVoisins[0][0], listeVoisins[0][1]);
					rep.addFils(rep2.getFils().elementAt(k));
				}
				rep2.getFils().removeAllElements();
				rep2.setPosRep(listeVoisins[0][0], listeVoisins[0][1]);
				rep.addFils(rep2);
				for(int k = 0; k < rep3.getFils().size(); ++k){
					rep3.getFils().elementAt(k).setPosRep(listeVoisins[0][0], listeVoisins[0][1]);
					rep.addFils(rep3.getFils().elementAt(k));
				}
				rep3.getFils().removeAllElements();
				rep3.setPosRep(listeVoisins[0][0], listeVoisins[0][1]);
				rep.addFils(rep3);

			}
		}
		else{
			System.out.println("Impossible cette case est déjà prise par un pion de couleur : " + getCasePlateau(i, j).getEtat());
			return false;
		}
		return true;
	}
	
	//fini
	public void afficheComposante(int i, int j, int c){
		int iRep = plateau[i][j].getPosIRep();
		int jRep = plateau[i][j].getPosJRep(); 
		for(int k = 0; k < plateau[iRep][jRep].getFils().size(); ++k){
			System.out.print(plateau[iRep][jRep].getFils().elementAt(k).getPosI());
			System.out.println(plateau[iRep][jRep].getFils().elementAt(k).getPosJ());
		}
		System.out.print(plateau[i][j].getPosI());
		System.out.println(plateau[i][j].getPosJ());
	}

	//fini
	public boolean existeCheminCases(Case c1, Case c2, int c){
		if (c1.getEtat() == c && c2.getEtat() == c){
			if(c1.getPosIRep() == c2.getPosIRep() && c1.getPosIRep() == c2.getPosJRep())
				return true;		
		}
		return false;
	}

	//fini
	public boolean existeCheminCotes(int c){
		if(c == 1){
			for(int j1 = 0; j1 < plateau.length; ++j1){
				for(int j2 = 0; j2 < plateau.length; ++j2){
					if(plateau[0][j1].getEtat() == plateau[plateau.length -1][j2].getEtat()){
						if(plateau[0][j1].getPosIRep() == plateau[plateau.length -1][j2].getPosIRep() &&
						plateau[0][j1].getPosJRep() == plateau[plateau.length -1][j2].getPosJRep()){
							return true;
						}
					}
				}
			}
		}
		if(c == 2){
			for(int i1 = 0; i1 < plateau.length; ++i1){
				for(int i2 = 0; i2 < plateau.length; ++i2){
					if(plateau[i1][0].getEtat() == plateau[i2][plateau.length -1].getEtat()){
						if(plateau[i1][0].getPosIRep() == plateau[i2][plateau.length -1].getPosIRep() &&
						plateau[i1][0].getPosJRep() == plateau[i2][plateau.length -1].getPosJRep()){
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	//fini mais surement à oprtimiser
	public boolean relieComposantes(int i, int j, int c){
		boolean ab = false;
		boolean bb = false;
		boolean cb = false;
		boolean db = false;
		boolean eb = false;
		boolean fb = false;
		int cpt = 0;
		if(i-1 >0){
			if(plateau[i-1][j].getEtat() == c){
				++cpt;
				ab = true;
			}
			if(j+1 <= plateau.length-1){
				if(plateau[i-1][j+1].getEtat() == c){
					++cpt;
					bb = true;
				}
			}
		}
		if(i+1 <= plateau.length){
			if(plateau[i+1][j].getEtat() == c){
				++cpt;
				db = true;
			}
			if(j-1 > 0){
				if(plateau[i+1][j-1].getEtat() == c){
					++cpt;
					eb = true;
				}
			}
		}
		if(j-1 >=0){
			if(plateau[i][j-1].getEtat() == c){
				++cpt;
				fb = true;
			}
		}
		if(j+1 <= plateau.length){
			if(plateau[i][j+1].getEtat() == c){
				++cpt;
				cb = true;
			}
		}

		if(cpt >= 2){
			if(cpt == 2){
				if(ab == true){
					if(cb == true){
						if(plateau[i-1][j].getPosIRep() != plateau[i][j+1].getPosIRep() ||
						plateau[i-1][j].getPosJRep() != plateau[i][j+1].getPosJRep()){
							return true;
						}
					}
					else if(db == true){
						if(plateau[i-1][j].getPosIRep() != plateau[i+1][j].getPosIRep() ||
						plateau[i-1][j].getPosJRep() != plateau[i+1][j].getPosJRep()){
							return true;
						}
					}
					else if(eb == true){
						if(plateau[i-1][j].getPosIRep() != plateau[i+1][j-1].getPosIRep() ||
						plateau[i-1][j].getPosJRep() != plateau[i+1][j-1].getPosJRep()){
							return true;
						}
					}
				}
				if(bb == true){
					if(db == true){
						if(plateau[i-1][j+1].getPosIRep() != plateau[i+1][j].getPosIRep() ||
						plateau[i-1][j+1].getPosJRep() != plateau[i+1][j].getPosJRep()){
							return true;
						}
					}
					else if(eb == true){
						if(plateau[i-1][j+1].getPosIRep() != plateau[i+1][j-1].getPosIRep() ||
						plateau[i-1][j+1].getPosJRep() != plateau[i+1][j-1].getPosJRep()){
							return true;
						}
					}
					else if(fb == true){
						if(plateau[i-1][j+1].getPosIRep() != plateau[i][j-1].getPosIRep() ||
						plateau[i-1][j+1].getPosJRep() != plateau[i][j-1].getPosJRep()){
							return true;
						}
					}
				}
				if(cb == true){
					if( eb == true){
						if(plateau[i][j+1].getPosIRep() != plateau[i+1][j-1].getPosIRep() ||
						plateau[i][j+1].getPosJRep() != plateau[i+1][j-1].getPosJRep()){
							return true;
						}
					}
					else if(fb == true){
						if(plateau[i][j+1].getPosIRep() != plateau[i][j-1].getPosIRep() ||
						plateau[i][j+1].getPosJRep() != plateau[i][j-1].getPosJRep()){
							return true;
						}
					}
				}
				if(db == true && fb == true){
					if(plateau[i+1][j].getPosIRep() != plateau[i][j-1].getPosIRep() ||
					plateau[i+1][j].getPosJRep() != plateau[i][j-1].getPosJRep()){
						return true;
					}
				}
			}
			else{
				if(ab == true && cb == true && eb == true){
					if(plateau[i-1][j].getPosIRep() != plateau[i][j+1].getPosIRep() ||
					plateau[i-1][j].getPosJRep() != plateau[i][j+1].getPosJRep() ||
					plateau[i-1][j].getPosIRep() != plateau[i+1][j+1].getPosIRep() ||
					plateau[i-1][j].getPosJRep() != plateau[i+1][j+1].getPosJRep() ||
					plateau[i][j+1].getPosIRep() != plateau[i+1][j+1].getPosIRep() ||
					plateau[i][j+1].getPosJRep() != plateau[i+1][j+1].getPosJRep()){
						return true;
					}
				}
				if(bb == true && db == true && fb == true){
					if(plateau[i-1][j+1].getPosIRep() != plateau[i+1][j].getPosIRep() ||
					plateau[i-1][j+1].getPosJRep() != plateau[i+1][j].getPosJRep() ||
					plateau[i-1][j+1].getPosIRep() != plateau[i][j-1].getPosIRep() ||
					plateau[i-1][j+1].getPosJRep() != plateau[i][j-1].getPosJRep() ||
					plateau[i+1][j].getPosIRep() != plateau[i][j-1].getPosIRep() ||
					plateau[i+1][j].getPosJRep() != plateau[i][j-1].getPosJRep()){
						return true;
					}
				}
			}
		}
		
		return false;
	}

	//fini
	public int calculeDistance(int i1, int j1, int i2, int j2){
		// Création du taleau qui calcul la distance
		int[][] tableau = new int[taille][taille];
		for(int i = 0; i < plateau.length; ++i){
			for(int j = 0; j < plateau.length; ++j){
				if(plateau[i][j].getEtat() != 0){
					tableau[i][j] = taille*taille;
				}
				else{
					tableau[i][j] = 0;
				}
				tableau[i1][j1] = 1;
				tableau[i2][j2] = -1;
			}
		}
		// Fin de création du tableau
		int cpt = 1;
		int i = 1;

		while(i != -1){
			for(int k = 0; k < plateau.length; ++k){
				for(int l = 0; l < plateau.length; ++l){
					if(tableau[k][l] == cpt){
						int[][] lVoisins = listeVoisins(k,l);
						++cpt;
						for(int m = 0; m < lVoisins.length; ++m){
							if(lVoisins[m][0] != -1){
								if(tableau[lVoisins[m][0]][lVoisins[m][1]] != -1){
									if(tableau[lVoisins[m][0]][lVoisins[m][1]] == 0){
										tableau[lVoisins[m][0]][lVoisins[m][1]] = cpt;
									}
								}
								else{
									i = -1;
								}
							}
						}
						--cpt;
					}
				}
			}
			++cpt;
		}


		return cpt -1;
	}

	public void joueDeuxHumains(){
		System.out.println("Le premier joueur doit relier le bord gauche et droit");
		System.out.println("Le deuxième joueur doit relier le bord haut et bas");
		int x, y;
		Scanner sc = new Scanner(System.in);
		String reponseUtilisateur;
		char resultat;
		System.out.println("Le joueur 1 commence à jouer");
		int j = 1;
		boolean fin = false;
		boolean erreur = false;
		while(fin == false){
			while(j == 1){
				menu();
				System.out.println("Veuillez placer un pion, votre couleur est le blanc représenté par le chiffre 1.");
				System.out.println("Entrez les coordonnées x et y de votre futur pion");

				reponseUtilisateur = sc.nextLine();
				if(reponseUtilisateur.length() != 3){
					System.out.println("Veuillez entrer vos coordonnées sous la forme : x,y");
					erreur = true;
				}
				else{
					resultat = reponseUtilisateur.charAt(0);
					x = Character.getNumericValue(resultat);
					resultat = reponseUtilisateur.charAt(2);
					y = Character.getNumericValue(resultat);;
					System.out.println("x : " + x + " y : " + y);
					if(!(x < 0 || x >= plateau.length || y < 0 || y >= plateau.length)){
						if(ajouterPion(x, y, 1) == true){
							afficherPlateau();
							erreur = false;
							if(existeCheminCotes(1) == true){
								System.out.println("Le joueur 1 a gagné!");
								fin = true;
							}													
						}
						else{
							erreur = true;
						}
					}
					else{
						System.out.println("La valeur que vous avez entré dépasse du tableau");
						erreur = true;
					}
				}
				if(!erreur){j = 2;}
			}
			while(j == 2 && fin == false){
				menu();
				System.out.println("Veuillez placer un pion, votre couleur est le noire représenté par le chiffre 2.");
				System.out.println("Entrez les coordonnées x et y de votre futur pion");

				reponseUtilisateur = sc.nextLine();
				if(reponseUtilisateur.length() != 3){
					System.out.println("Veuillez entrer vos coordonnées sous la forme : x,y");
					erreur = true;
				}
				else{
					resultat = reponseUtilisateur.charAt(0);
					x = Character.getNumericValue(resultat);
					resultat = reponseUtilisateur.charAt(2);
					y = Character.getNumericValue(resultat);
					System.out.println("x : " + x + " y : " + y);
					if(!(x < 0 || x >= plateau.length || y < 0 || y >= plateau.length)){
						if(ajouterPion(x, y, 2) == true){
							afficherPlateau();
							erreur = false;
							if(existeCheminCotes(2) == true){
								System.out.println("Le joueur 2 a gagné!");
								fin = true;
							}
						}
						else{
							erreur = true;
						}
					}
					else{
						System.out.println("La valeur que vous avez entré dépasse du tableau");
						erreur = true;
					}
				}
				if(!erreur){j = 1;}
			}
		}
	}

	public int evaluerPionZ(){
		return 1;
	}

	public void joueOrdiHumain(){
		System.out.println("Vous êtes jouez avec le pion 1, votre but est de relier le bord gauche et droite");
		System.out.println("L'ordinateur est le joueur 2, son but est de relier le bord haut et bas");
		int x, y;
		Scanner sc = new Scanner(System.in);
		String reponseUtilisateur;
		char resultat;
		System.out.println("Le joueur 1 commence à jouer");
		int j = 1;
		boolean fin = false;
		boolean erreur = false;
		int dernierI = -1;
		int dernierJ = -1;
		int dernierIO = -1;
		int dernierJO = -1;
		int compteurTour = 1;
		while(fin == false){
			while(j == 1){
				System.out.println("Veuillez placer un pion, votre couleur est le blanc représenté par le chiffre 1.");
				System.out.println("Entrez les coordonnées x et y de votre futur pion");

				reponseUtilisateur = sc.nextLine();
				if(reponseUtilisateur.length() != 3){
					System.out.println("Veuillez entrer vos coordonnées sous la forme : x,y");
					erreur = true;
				}
				else{
					resultat = reponseUtilisateur.charAt(0);
					x = Character.getNumericValue(resultat);
					resultat = reponseUtilisateur.charAt(2);
					y = Character.getNumericValue(resultat);;
					System.out.println("x : " + x + " y : " + y);
					if(!(x < 0 || x >= plateau.length || y < 0 || y >= plateau.length)){
						if(ajouterPion(x, y, 1) == true){
							dernierI = x;
							dernierJ = y;
							afficherPlateau();
							erreur = false;
							if(existeCheminCotes(1) == true){
								System.out.println("Le joueur 1 a gagné!");
								fin = true;
							}													
						}
						else{
							erreur = true;
						}
					}
					else{
						System.out.println("La valeur que vous avez entré dépasse du tableau");
						erreur = true;
					}
				}
				if(!erreur){j = 2;}
			}
			while(j == 2 && fin == false){
				Random rdm = new Random();
				
				int rdmRes;
				if(compteurTour == 1){
					rdmRes = (Math.abs(rdm.nextInt())) % (plateau.length -1) +1;
					ajouterPion(rdmRes,0,2);
					dernierIO = rdmRes;
					dernierJO = 0;
					System.out.println("Le joueur 2 à jouer à la position (" + dernierIO + "," + dernierJO + ")");
					afficherPlateau();
				}
				else{
					rdmRes = Math.abs(rdm.nextInt()) % 2;
					if(rdmRes == 0){
						int[][] lVoisins = listeVoisins(dernierIO, dernierJO);
						Vector<Case> tableau = new Vector<Case>();	
						for(int i = 0; i < lVoisins.length; ++i){
							if(lVoisins[i][0] != -1){
								if(plateau[lVoisins[i][0]][lVoisins[i][1]].getEtat() == 0){
									tableau.add(plateau[lVoisins[i][0]][lVoisins[i][1]]);
								}
							}
						}
						rdmRes = Math.abs(rdm.nextInt()) % tableau.size();
						dernierIO = tableau.elementAt(rdmRes).getPosI();
						dernierJO = tableau.elementAt(rdmRes).getPosJ();
						ajouterPion(dernierIO, dernierJO,2);
						System.out.println("Le joueur 2 à jouer à la position (" + dernierIO + "," + dernierJO + ")");
						afficherPlateau();
						if(existeCheminCotes(2) == true){
							System.out.println("Le joueur 2 à gagné");
							fin = true;
						}
					}
					else{
						int[][] lVoisins = listeVoisins(dernierI, dernierJ);
						Vector<Case> tableau = new Vector<Case>();	
						for(int i = 0; i < lVoisins.length; ++i){
							if(lVoisins[i][0] != -1){
								if(plateau[lVoisins[i][0]][lVoisins[i][1]].getEtat() == 0){
									tableau.add(plateau[lVoisins[i][0]][lVoisins[i][1]]);
								}
							}
						}
						rdmRes = Math.abs(rdm.nextInt()) % tableau.size();
						ajouterPion(tableau.elementAt(rdmRes).getPosI(), tableau.elementAt(rdmRes).getPosJ(),2);
						System.out.println("Le joueur 2 à jouer à la position (" + tableau.elementAt(rdmRes).getPosI() + "," + tableau.elementAt(rdmRes).getPosJ() + ")");
						afficherPlateau();
						if(existeCheminCotes(2) == true){
							System.out.println("Le joueur 2 à gagné");
							fin = true;
						}
					}
				}
				++compteurTour;
				j = 1;
			}
		}
	}

	public int getTaille(){
		return taille;
	}

	public void setTaille(int t){
		taille = t;
	}
	public Case getCasePlateau(int i, int j){
		return plateau[i][j];
	}
	public Case[][] getCasePlateau(){
		return plateau;
	}

	//////////////////////////////////////////////////////////////
	//Méthodes peronnelles
	//fini
	public void creerTableau(){
		plateau = new Case[taille][taille];
		System.out.println("");
		for(int i = 0; i < plateau.length; ++i){
			for(int j = 0; j < plateau[i].length; ++j){
				plateau[i][j] = new Case(0,i,j,i,j);
			}
		}	
	}

	//fini
	public int[][] voisins(int i, int j, int c){
		int[][] vec = {{-1,-1},{-1,-1},{-1,-1}};
		int x = 0;
		int y;
		boolean present = false;
		if(i-1 >= 0){
			if(plateau[i-1][j].getEtat() == c){
				vec[x][0] = plateau[i-1][j].getPosIRep();
				vec[x][1] = plateau[i-1][j].getPosJRep();
				++x;
			}
			if(j+1 <= (plateau.length-1)){
				if(plateau[i-1][j+1].getEtat() == c){
					for(y = 0; y < x; ++y){
						if((vec[y][0] == plateau[i-1][j+1].getPosIRep()) &&
						(vec[y][1] == plateau[i-1][j+1].getPosJRep())){
							y = x;
							present = true;
						}
					}
					if(!present){
						vec[x][0] = plateau[i-1][j+1].getPosIRep();
						vec[x][1] = plateau[i-1][j+1].getPosJRep();
						++x;
					}
					else{
						present = false;
					}
				}
			}
		}
		if(i+1 <= (plateau.length-1) && j-1 >= 0){
			if(plateau[i+1][j].getEtat() == c){
				for(y = 0; y < x; ++y){
					if((vec[y][0] == plateau[i+1][j].getPosIRep()) &&
					(vec[y][1] == plateau[i+1][j].getPosJRep())){
						y = x;
						present = true;
					}
				}
				if(!present){
					vec[x][0] = plateau[i+1][j].getPosIRep();
					vec[x][1] = plateau[i+1][j].getPosJRep();
					++x;
				}
				else{
					present = false;
				}
			}
			if(j-1 <= (plateau.length-1)){
				if(plateau[i+1][j-1].getEtat() == c){
					for(y = 0; y < x; ++y){
						if((vec[y][0] == plateau[i+1][j-1].getPosIRep()) &&
						(vec[y][1] == plateau[i+1][j-1].getPosJRep())){
							y = x;
							present = true;
						}
					}
					if(!present){
						vec[x][0] = plateau[i+1][j-1].getPosIRep();
						vec[x][1] = plateau[i+1][j-1].getPosJRep();
						++x;
					}
					else{
						present = false;
					}
				}
			}
		}
		if(j-1 >= 0){
			if(plateau[i][j-1].getEtat() == c){
				for(y = 0; y < x; ++y){
					if((vec[y][0] == plateau[i][j-1].getPosIRep()) &&
					(vec[y][1] == plateau[i][j-1].getPosJRep())){
						y = x;
						present = true;
					}
				}
				if(!present){
					vec[x][0] = plateau[i][j-1].getPosIRep();
					vec[x][1] = plateau[i][j-1].getPosJRep();
					++x;
				}
				else{
					present = false;
				}
			}
		}
		if(j+1 <= (plateau.length-1)){
			if(plateau[i][j+1].getEtat() == c){
				for(y = 0; y < x; ++y){
					if((vec[y][0] == plateau[i-1][j+1].getPosIRep()) &&
					(vec[y][1] == plateau[i-1][j+1].getPosJRep())){
						y = x;
						present = true;
					}
				}
				if(!present){
					vec[x][0] = plateau[i][j+1].getPosIRep();
					vec[x][1] = plateau[i][j+1].getPosJRep();
					++x;
				}
				else{
					present = false;
				}
			}
		}
		return vec;
	}

	public int[][] listeVoisins(int i, int j){
		int[][] tab = {{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1},{-1,-1}};
		if(i == 0){
			if(j == 0){
				tab[2][0] = i;
				tab[2][1] = j+1;
				tab[3][0] = i+1;
				tab[3][1] = j;
			}
			else if(j == plateau.length -1){
				tab[3][0] = i+1;
				tab[3][1] = j;
				tab[4][0] = i+1;
				tab[4][1] = j-1;
				tab[5][0] = i;
				tab[5][1] = j-1;
			}
			else{
				tab[2][0] = i;
				tab[2][1] = j+1;
				tab[3][0] = i+1;
				tab[3][1] = j;
				tab[4][0] = i+1;
				tab[4][1] = j-1;
				tab[5][0] = i;
				tab[5][1] = j-1;
			}
		}
		else if(i == plateau.length -1){
			if(j == 0){
				tab[0][0] = i-1;
				tab[0][1] = j;
				tab[1][0] = i-1;
				tab[1][1] = j+1;
				tab[2][0] = i;
				tab[2][1] = j+1;
			}
			else if(j == plateau.length -1){
				tab[0][0] = i-1;
				tab[0][1] = j;
				tab[5][0] = i;
				tab[5][1] = j-1;
			}
			else{
				tab[0][0] = i-1;
				tab[0][1] = j;
				tab[1][0] = i-1;
				tab[1][1] = j+1;
				tab[2][0] = i;
				tab[2][1] = j+1;
				tab[5][0] = i;
				tab[5][1] = j-1;
			}
		}
		else if(j == 0){
			tab[0][0] = i-1;
			tab[0][1] = j;
			tab[1][0] = i-1;
			tab[1][1] = j+1;
			tab[2][0] = i;
			tab[2][1] = j+1;
			tab[3][0] = i+1;
			tab[3][1] = j;
		}
		else if(j == plateau.length -1){
			tab[3][0] = i+1;
			tab[3][1] = j;
			tab[4][0] = i+1;
			tab[4][1] = j-1;
			tab[5][0] = i;
			tab[5][1] = j-1;
			tab[1][0] = i-1;
			tab[1][1] = j;
		}
		else{
			tab[0][0] = i-1;
			tab[0][1] = j;
			tab[1][0] = i-1;
			tab[1][1] = j+1;
			tab[2][0] = i;
			tab[2][1] = j+1;
			tab[3][0] = i+1;
			tab[3][1] = j;
			tab[4][0] = i+1;
			tab[4][1] = j-1;
			tab[5][0] = i;
			tab[5][1] = j-1;
		}
		return tab;

	}

	//fini
	//pour test
	public void changerTousRepresentants(int i1, int j1, int i2, int j2){
		plateau[i2][j2].setPosRep(i1, j1);
		for (Case c : plateau[i2][j2].getFils()) {
			plateau[c.getPosI()][c.getPosJ()].setPosRep(i1,j1);
			plateau[i1][j1].getFils().add(c);
			plateau[i2][j2].getFils().remove(c);
		}
		plateau[i1][j1].getFils().add(plateau[i2][j2]);
	}

	//Méthodes d'affichage
	//fini
	public void afficherPlateau(){
		String space = "";
		int cpt = 0;
		while(cpt < plateau.length){
			System.out.print(space);
			for(int j = 0; j < 4*plateau.length; ++j){
				System.out.print("-");
			}
			System.out.println();
			System.out.print(space);
			System.out.print("|");
			for(int i = 0; i < plateau.length; ++i){
				System.out.print(" " + plateau[i][cpt].getEtat() + " |");
			}
			System.out.println();
			System.out.print(space);
			for(int j = 0; j < 4*plateau.length; ++j){
				System.out.print("-");
			}
			++cpt;
			space += "  ";
			System.out.println();
		}
	}

	//fini
	// pour test
	public void afficherPlateauRepresentants(){
		String space = "";
		int cpt = 0;
		while(cpt < plateau.length){
			System.out.print(space);
			for(int j = 0; j < 4*plateau.length; ++j){
				System.out.print("-");
			}
			System.out.println();
			System.out.print(space);
			System.out.print("|");
			for(int i = 0; i < plateau.length; ++i){
				System.out.print(" " + plateau[i][cpt].getPosIRep() + plateau[i][cpt].getPosJRep() +" |");
			}
			System.out.println();
			System.out.print(space);
			for(int j = 0; j < 4*plateau.length; ++j){
				System.out.print("-");
			}
			++cpt;
			space += "  ";
			System.out.println();
		}
	}

	public void menu(){
		Scanner sc = new Scanner(System.in);
		String param, choix;
		char resultat;
		int t, w, x, y, z;
		do{
			System.out.println("Que voulez-vous faire?");
			System.out.println("1) ajouterPion");
			System.out.println("2) afficheComposante");
			System.out.println("3) existeCheminCases");
			System.out.println("4) existeCheminCotes");
			System.out.println("5) relieComposantes");
			System.out.println("6) calculeDistance");

			choix = sc.nextLine();
			if(choix.length() != 1){
				System.out.println("Erreur entrez UN chiffre");
				choix = sc.nextLine();
			}
			t = Integer.parseInt(choix);
			switch(t){
			case 1:
				break;
			case 2:
				do{
					System.out.println("Veuillez entrer les coordonnées x,y du pion ainsi que sa couleur");
					System.out.println("Sous la forme x,y,c");
					param = sc.nextLine();
				}
				while(param.length() != 5);
				resultat = param.charAt(0);
				x = Character.getNumericValue(resultat);
				resultat = param.charAt(2);
				y = Character.getNumericValue(resultat);
				resultat = param.charAt(4);
				z = Character.getNumericValue(resultat);
				afficheComposante(x, y, z);
				break;
			case 3:
				do{
					System.out.println("Veuillez entrer les coordonnées x,y du premier pion");
					System.out.println("Sous la forme x,y");
					param = sc.nextLine(); 
				}
				while(param.length() != 3);
				resultat = param.charAt(0);
				x = Character.getNumericValue(resultat);
				resultat = param.charAt(2);
				y = Character.getNumericValue(resultat);
				Case c1 = plateau[x][y];
				do{
					System.out.println("Veuillez entrer les coordonnées x,y du deuxième pion");
					System.out.println("Sous la forme x,y");
					param = sc.nextLine(); 
				}
				while(param.length() != 3);
				resultat = param.charAt(0);
				x = Character.getNumericValue(resultat);
				resultat = param.charAt(2);
				y = Character.getNumericValue(resultat);
				Case c2 = plateau[x][y];
				do{
					System.out.println("Veuillez entrer la couleur du pion");
					System.out.println("Sous la forme c");
					param = sc.nextLine(); 
				}
				while(param.length() != 1);
				resultat = param.charAt(0);
				z = Character.getNumericValue(resultat);
				if(existeCheminCases(c1,c2,z)){
					System.out.println("vrai");
				}
				else{
					System.out.println("faux");
				}
				break;
			case 4:
				do{
					System.out.println("Veuillez entrer la couleur du pion");
					System.out.println("Sous la forme c");
					param = sc.nextLine(); 
				}
				while(param.length() != 1);
				resultat = param.charAt(0);
				z = Character.getNumericValue(resultat);
				if(existeCheminCotes(z)){
					System.out.println("vrai");
				}
				else{
					System.out.println("faux");
				}
				break;
			case 5:
				do{
					System.out.println("Veuillez entrer les coordonnées x,y du pion ainsi que sa couleur");
					System.out.println("Sous la forme x,y,c");
					param = sc.nextLine();
				}
				while(param.length() != 5);
				resultat = param.charAt(0);
				x = Character.getNumericValue(resultat);
				resultat = param.charAt(2);
				y = Character.getNumericValue(resultat);
				resultat = param.charAt(4);
				z = Character.getNumericValue(resultat);
				if(relieComposantes(x, y, z)){
					System.out.println("vrai");
				}
				else{
					System.out.println("faux");
				}
				break;
			case 6:
				do{
					System.out.println("Veuillez entrer les coordonnées x,y du premier pion");
					System.out.println("Sous la forme x,y");
					param = sc.nextLine(); 
				}
				while(param.length() != 3);
				resultat = param.charAt(0);
				x = Character.getNumericValue(resultat);
				resultat = param.charAt(2);
				y = Character.getNumericValue(resultat);
				do{
					System.out.println("Veuillez entrer les coordonnées x,y du deuxième pion");
					System.out.println("Sous la forme x,y");
					param = sc.nextLine(); 
				}
				while(param.length() != 3);
				resultat = param.charAt(0);
				w = Character.getNumericValue(resultat);
				resultat = param.charAt(2);
				z = Character.getNumericValue(resultat);
				System.out.println(calculeDistance(x,y,w,z));
				break;
			default:
				System.out.println("Erreur!");
				break;
			}
		}
		while(t != 1);
		
	}
}